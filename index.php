<?php
require_once 'Mobile_Detect.php';
$detect = new Mobile_Detect;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <?php include_once ("includes/head.php") ?>
    <?php

    if( $detect->isMobile() && !$detect->isTablet() ){ ?>
	<script>window.location.href="http://www.selvaviva.cl/m/"</script>
        
    <?php } ?>

    <script type="text/javascript">
        $(document).ready(function(){
            var imagenes = [
                "imgs/banners/<?php echo $bnr["url_imagen_1"]?>",
                "imgs/banners/<?php echo $bnr["url_imagen_2"]?>",
                "imgs/banners/<?php echo $bnr["url_imagen_3"]?>"
            ];
            var links = [
                "<?php echo $bnr["url_link_1"]?>",
                "<?php echo $bnr["url_link_2"]?>",
                "<?php echo $bnr["url_link_3"]?>"
            ];

            var promos = [
                "imgs/banners/<?php echo $bnr["promo_1"]?>",
                "imgs/banners/<?php echo $bnr["promo_2"]?>",
                "imgs/banners/<?php echo $bnr["promo_3"]?>"
            ];

            // Precargar todas la imagenes
            $(imagenes).each(function(){
                $("<img/>")[0].src = this;
            });


            var index = 0;
            setInterval(function() {
                index = (index >= imagenes.length - 1) ? 0 : index + 1;
                $.background(index);
            }, 9000);



            $("#fondo_1").click(function(e){
                e.preventDefault();
                $.background(0);
               // $("#img").attr("src","imgs/banners/<?php echo $bnr["promo_1"]?>");
               // $("#link").attr("href","<?php echo $bnr["url_link_1"]?>");
            });

            $("#fondo_2").click(function(e){
                e.preventDefault();
                $.background(1);
               // $("#img").attr("src","imgs/banners/<?php echo $bnr["promo_2"]?>");
                //$("#link").attr("href","<?php echo $bnr["url_link_2"]?>");
            });
            $("#fondo_3").click(function(e){
                e.preventDefault();
                $.background(2);
               // $("#img").attr("src","imgs/banners/<?php echo $bnr["promo_3"]?>");
               // $("#link").attr("href","<?php echo $bnr["url_link_3"]?>");
            });

            $.background = function(index){
                url="url(" + imagenes[index] +")";
                $(principal).css({'background-image': url});
                $("#img").attr("src",promos[index]);
                $("#link").attr("href",links[index]);
            }


            $("#principal").click(function(e){
                //alert("ok");
            })
        });
    </script>


    <title>SELVA</title>
</head>
<body>

<div id="principal" style="min-height: 800px; background-image:url('imgs/banners/<?php echo $bnr["url_imagen_1"]?>'); background-size: cover">
    <div id="promo">
        <a id="link" href="<?php echo $bnr["url_link_1"]?>" target="_blank"><img id="img" src="imgs/<?php echo $bnr["promo_1"]?>"/></a>
    </div>
    <div id="contenido-header">
        <?php
        $op_m=1;
        include_once("includes/menu_header.php");
        ?>
    </div>
    <!--div id="banner">
    </div>
    <div id="botones">
    </div-->



    <div id="down">
        <div id="down_1">
            <table border="0" id="banner" align="center">
                <tr>
                    <td align="center" valign="middle">
                    </td>
                    <td align="center" valign="bottom">
                        <div id="botones">
                            <div class="icon" id="fondo_1"><img src="imgs/banners/<?php echo $bnr["url_icon_1"]?>" alt=""/></div>
                            <div class="icon" id="fondo_2"><img src="imgs/banners/<?php echo $bnr["url_icon_2"]?>" alt=""/></div>
                            <div class="icon" id="fondo_3"><img src="imgs/banners/<?php echo $bnr["url_icon_3"]?>" alt=""/></div>
                        </div>
                    </td>
                    <td align="center" valign="middle">
                    </td>
                </tr>
            </table>
        </div>
        <div id="down_2">
            <div id="panel">
                <table border="0" align="center">
                    <tr>
                        <td rowspan="3" valign="top"><a href="<?php echo $bnr['link_imagen_1']?>"  target="_blank">
                            <img src="imgs/banners/<?php echo $bnr['imagen_1']?>" style="width: 100%" /></a> </td>
                        <td rowspan="2" align="center" valign="top"><a href="<?php echo $bnr['link_imagen_2']?>">
                            <img src="imgs/banners/<?php echo $bnr['imagen_2']?>" /></a> </td>
                        <td rowspan="2" align="center" valign="top"><a href="<?php echo $bnr['link_imagen_3']?>">
                            <img src="imgs/banners/<?php echo $bnr['imagen_3']?>" /></a></td>
                        <td valign="top" align="right"><a href="<?php echo $bnr['link_imagen_4']?>">
                            <img src="imgs/banners/<?php echo $bnr['imagen_4']?>"/></a></td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="right" style="padding-bottom: 5px;">
                            <a href="<?php echo $bnr['link_imagen_5']?>"><img src="imgs/banners/<?php echo $bnr['imagen_5']?>"/></a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" valign="bottom"><img src="imgs/direccion-footer.png" style="width: 100%"/></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <div style="width: auto; margin: auto">
                                <div id="patrocinador_in">
                                    <?php foreach($lst_ptr as $p){?>
                                        <a href='<?php echo $p['url']?>' target="_blank"><img src="imgs/sponsors/<?php echo $p['icono']?>" style="height: 55px; width: auto"></a>
                                    <?php }?>
                                </div>
                                <div id="sponsor_in">
                                    <?php foreach($lst_spn as $s){?>
                                        <a href='<?php echo $s['url']?>' target="_blank"><img src="imgs/sponsors/<?php echo $s['icono']?>" style="height: 55px; width: auto"></a>
                                    <?php }?>
                                </div>
                                <div id="partner_in">
                                    <?php foreach($lst_prt as $p){?>
                                        <a href='<?php echo $p['url']?>' target="_blank"><img src="imgs/sponsors/<?php echo $p['icono']?>" style="height: 55px; width: auto"></a>
                                    <?php }?>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</html>