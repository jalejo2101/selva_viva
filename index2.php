<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta charset="utf-8"><link rel="icon" type="image/png" href="imgs/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/jquery.backstretch.min.js"></script>
    <link rel="stylesheet" href="css/style.css"/>
    <title></title>
</head>
<body>
<div id="main">
    <div id="header">
        <div id="logo"><img src="imgs/logo-selva_LT.png" alt=""/></div>
        <div id="redes">
            <img src="imgs/siguenos.png" style="margin: 3px 0px 0px 20px"/>
            <div>
                <img src="imgs/btn-fb.png" alt=""/>
                <img src="imgs/btn-tw.png" alt=""/>
                <img src="imgs/btn-inst.png" alt=""/>
                <img src="imgs/btn-youtube.png" alt=""/>
            </div>
        </div>
        <div id="menu-h">
            <div class="m-item" id="btn-home"></div>
            <div class="m-item" id="btn-que_es"></div>
            <div class="m-item" id="btn-como_llegar"></div>
            <div class="m-item" id="btn-visitanos"></div>
            <div class="m-item" id="btn-noticias"></div>
            <div class="m-item" id="btn-contact"></div>
        </div>
    </div>
    <div id="contenido">
    </div>
</div>
<div id="footer">
    <table><tr><td align="left" valign="bottom">
        <?php include_once("includes/footer_promo.php")?>
    </td></tr></table>
</div>

</body>
</html>