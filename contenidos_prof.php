<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <?php include_once ("includes/head.php") ?>
    <?php
    $pdfs=$con->get_documentos();

    ?>

    <title></title>
    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>
</head>
<body>
 <div id="principal">
    <!---------------------------------------------------------------------------->

     <div  id="img_sup_izq">
         <img src="imgs/banners/<?php echo $bnr["sup_izq_4"]?>"/>
     </div>
     <div  id="img_inf_der">
         <img src="imgs/banners/<?php echo $bnr["inf_der_4"]?>"/>
     </div>


    <!---------------------------------------------------------------------------->
    <div id="header">
        <?php
        $op_m=5;
        include_once("includes/menu_header.php");
        ?>
    </div>

    <div id="main">
        <div class="margen-sup"></div>
            <div id="frame">
                <table border="0">
                    <tr>
                        <td valign="top" align="right">
                            <?php
                            $op="4";
                            include_once("includes/menu_colegios.php")?>
                        </td>
                        <td>
                            <div id="text">
                                <h2>Contenidos para el Profesor</h2>
                                <div id="temp"></div>
                                <p>
                                    ¡Pong&aacute;mos en pr&aacute;ctica lo que aprendimos! 
                                </p>

                                <p>Contamos con guías educativas según su nivel (1º básico a 8° básico). En ellas se da información sobre la naturaleza, los seres vivos y además posee una sección de ejercicios, donde el alumno podrá&nbsp;poner en práctica sus conocimientos.</p>
<div class="verde">
                  Documento
              </div>
                                <?php foreach($pdfs as $pdf){?>
                                <div class="verde">
                                    <div style="width: 370px;float: left"><?php echo $pdf["descripcion"]?></div>
                                    <a href="files/<?php echo $pdf["documento"]?>">
                                        <img src="imgs/btn-descargar.png" alt="" align="middle" style="float: left"/>
                                    </a>

                                </div>
                                <?php }?>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
    </div>
    <div id="footer">
        <table><tr><td align="left" valign="bottom">
            <?php include_once("includes/footer_promo.php")?>
        </td></tr></table>
    </div>
 </div>
<script type="text/javascript">
    $(document).ready(function(){
        var x= $(window).height();
        $("#text").height(x-250);
        $(window).resize(function(){
            //$("#temp").text($("#text").height());
            var x= $(window).height();
            $("#text").height(x-240);

           //alert("ok");
        });
    });
</script>

</body>
</html>


