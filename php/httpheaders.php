<?php
	header("Vary: Accept");

	if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml") && !stristr($_SERVER['HTTP_USER_AGENT'], 'Safari'))
		header("Content-Type: application/xhtml+xml; charset=utf-8");
	else
		header("Content-Type: text/html; charset=utf-8");
?>