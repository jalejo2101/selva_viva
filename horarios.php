<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <?php include_once ("includes/head.php") ?>

    <link rel="stylesheet" href="css/style.css"/>

    <title></title>
    <script type="text/javascript">
        $(document).ready(function(){




            $("#titulo1").click(function(){
                if($("#titulo1").next().is(":visible")) {
                    $("#titulo1").next().slideUp();
                    $("#titulo1").css("background-image","url('imgs/btn-horario.png')");
                }else{
                    $("#titulo1").css("background-image","url('imgs/btn-horario-hover.png')");
                    $(".conten").each(function() {
                            $(this).slideUp();
                    });
                    $("#titulo1").next().slideDown();
                    $("#titulo2").css("background-image","url('imgs/btn-precios.png')");
                    $("#titulo3").css("background-image","url('imgs/btn-formas.png')");
                }
            })

            $("#titulo2").click(function(){
                if($("#titulo2").next().is(":visible")) {
                    $("#titulo2").next().slideUp();
                    $("#titulo2").css("background-image","url('imgs/btn-precios.png')");
                }else{
                    $("#titulo2").css("background-image","url('imgs/btn-precios-hover.png')");
                    $(".conten").each(function() {
                            $(this).slideUp();
                    });
                    $("#titulo2").next().slideDown();
                    $("#titulo1").css("background-image","url('imgs/btn-horario.png')");
                    $("#titulo3").css("background-image","url('imgs/btn-formas.png')");
                }
            })

            $("#titulo3").click(function(){
                if($("#titulo3").next().is(":visible")) {
                    $("#titulo3").next().slideUp();
                    $("#titulo3").css("background-image","url('imgs/btn-formas.png')");
                }else{
                    $("#titulo3").css("background-image","url('imgs/btn-formas-hover.png')");
                    $(".conten").each(function() {
                        if(!$(this).hasClass("activo")){
                            $(this).slideUp();
                        }
                    });
                    $("#titulo3").next().slideDown();
                    $("#titulo1").css("background-image","url('imgs/btn-horario.png')");
                    $("#titulo2").css("background-image","url('imgs/btn-precios.png')");

                }
            })



        });
    </script>

    <style>

    </style>


</head>
<body>
 <div id="principal">
    <!---------------------------------------------------------------------------->

     <div  id="img_sup_izq">
         <img src="imgs/banners/<?php echo $bnr["sup_izq_2"]?>"/>
     </div>
     <div  id="img_inf_der">
         <img src="imgs/banners/<?php echo $bnr["inf_der_2"]?>"/>
     </div>



    <!---------------------------------------------------------------------------->
    <div id="header">
        <?php
        $op_m=3;
        include_once("includes/menu_header.php");
        ?>
    </div>

    <div id="main">
        <div class="margen-sup"></div>
            <div id="frame">
                <table border="0">
                    <tr>
                        <td valign="top" align="right">
                            <?php
                            $op="1";
                            include_once("includes/menu_comollegar.php")?>
                        </td>
                        <td>
                            <div id="text">
                                <h2>Horarios y Tarifas</h2><div id="temp"></div>
                                <div style="width: 550px; margin: auto">
                                    <div class="acordeon">
                                        <div id="titulo1" class="tit">
                                        </div>
                                        <div class="conten">

                                            <?php echo $contenido["html_horarios"]?>

                                        </div>
                                        <div id="titulo2" class="tit">
                                        </div>
                                        <div class="conten">
                                            <?php echo $contenido["html_precios"]?>
                                        </div>
                                        <div id="titulo3" class="tit">
                                        </div>
                                        <div class="conten">
                                            <?php echo $contenido["html_forma_pago"]?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
    </div>
    <div id="footer">
        <table><tr><td align="left" valign="bottom">
            <?php include_once("includes/footer_promo.php")?>
        </td></tr></table>
    </div>
 </div>
<script type="text/javascript">
    $(document).ready(function(){
        var x= $(window).height();
        $("#text").height(x-250);
        $(window).resize(function(){
            //$("#temp").text($("#text").height());
            var x= $(window).height();
            $("#text").height(x-240);

           //alert("ok");
        });
    });
</script>

</body>
</html>


