<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <?php include_once ("includes/head.php") ?>
    <?php $noti=$con->get_noticia($_GET["id"])?>
    <?php $mes=array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre")?>

    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>
</head>
<body>
 <div id="principal">
    <!---------------------------------------------------------------------------->

     <div  id="img_sup_izq">
         <img src="imgs/banners/<?php echo $bnr["sup_izq_6"]?>"/>
     </div>
     <div  id="img_inf_der">
         <img src="imgs/banners/<?php echo $bnr["inf_der_6"]?>"/>
     </div>


    <!---------------------------------------------------------------------------->
    <div id="header">
        <?php
        $op_m=7;
        include_once("includes/menu_header.php");
        ?>
    </div>

    <div id="main">
        <div class="margen-sup"></div>
            <div id="frame">
                <table border="0">
                    <tr>
                        <td valign="top" align="right">
                            <?php
                            $op="1";
                            //include_once("includes/menu_comollegar.php")?>
                        </td>
                        <td>
                            <div id="text" style="min-width: 780px">
                                <h2><?php echo $noti["titulo"]?></h2>
                                <div id="temp"></div>
                                <table id="noticias">
                                    <tr>
                                        <td style="width: 200px; min-width: 200px" >
                                            <?php $m= substr($noti["fecha"],8,2)." de "
                                                .$mes[intval(substr($noti["fecha"],5,2))]." de ".substr($noti["fecha"],0,4)?>
                                            <?php echo $m?>
                                            <br>
                                            <img src="imgs/noti/<?php echo $noti["foto"]?>" alt="" style="width: 500px"/>
                                        </td>
                                    <tr>

                                    </tr>
                                        <td style="width: 320px; font-size: 14px">
                                            <?php echo $noti["descripcion"]?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a  href="noticias.php">
                                                <img src="imgs/btn-volver.png" alt="" style="width: 80px"/>
                                            </a>
                                        </td>
                                    </tr>
                                </table>




                            </div>
                        </td>
                    </tr>
                </table>
            </div>
    </div>
    <div id="footer">
        <table><tr><td align="left" valign="bottom">
            <?php include_once("includes/footer_promo.php")?>
        </td></tr></table>
    </div>
 </div>
<script type="text/javascript">
    $(document).ready(function(){
        var x= $(window).height();
        $("#text").height(x-250);
        $(window).resize(function(){
            //$("#temp").text($("#text").height());
            var x= $(window).height();
            $("#text").height(x-240);

           //alert("ok");
        });
    });
</script>

</body>
</html>


