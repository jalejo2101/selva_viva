-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 16-06-2015 a las 02:03:02
-- Versión del servidor: 5.6.23
-- Versión de PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `selva_viva`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `password` varchar(45) NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1001 ;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`iduser`, `user`, `name`, `email`, `password`, `level`) VALUES
(1000, 'admin', 'Administrador', NULL, 'e10adc3949ba59abbe56e057f20f883e', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `id` int(11) NOT NULL,
  `url_imagen_1` varchar(45) DEFAULT NULL,
  `url_icon_1` varchar(45) DEFAULT NULL,
  `url_link_1` varchar(128) DEFAULT 'null',
  `promo_1` varchar(65) DEFAULT NULL,
  `url_imagen_2` varchar(45) DEFAULT NULL,
  `url_icon_2` varchar(45) DEFAULT NULL,
  `url_link_2` varchar(128) DEFAULT 'null',
  `promo_2` varchar(65) DEFAULT NULL,
  `url_imagen_3` varchar(45) DEFAULT NULL,
  `url_icon_3` varchar(45) DEFAULT NULL,
  `url_link_3` varchar(128) DEFAULT NULL,
  `promo_3` varchar(65) DEFAULT NULL,
  `imagen_1` varchar(45) DEFAULT NULL,
  `link_imagen_1` varchar(128) DEFAULT NULL,
  `imagen_2` varchar(45) DEFAULT NULL,
  `link_imagen_2` varchar(128) DEFAULT NULL,
  `imagen_3` varchar(45) DEFAULT NULL,
  `link_imagen_3` varchar(128) DEFAULT NULL,
  `imagen_4` varchar(45) DEFAULT NULL,
  `link_imagen_4` varchar(128) DEFAULT NULL,
  `imagen_5` varchar(45) DEFAULT NULL,
  `link_imagen_5` varchar(128) DEFAULT NULL,
  `sup_izq_1` varchar(45) DEFAULT NULL,
  `inf_der_1` varchar(45) DEFAULT NULL,
  `sup_izq_2` varchar(45) DEFAULT NULL,
  `inf_der_2` varchar(45) DEFAULT NULL,
  `sup_izq_3` varchar(45) DEFAULT NULL,
  `inf_der_3` varchar(45) DEFAULT NULL,
  `sup_izq_4` varchar(45) DEFAULT NULL,
  `inf_der_4` varchar(45) DEFAULT NULL,
  `sup_izq_5` varchar(45) DEFAULT NULL,
  `inf_der_5` varchar(45) DEFAULT NULL,
  `sup_izq_6` varchar(45) DEFAULT NULL,
  `inf_der_6` varchar(45) DEFAULT NULL,
  `sup_izq_7` varchar(45) DEFAULT NULL,
  `inf_der_7` varchar(45) DEFAULT NULL,
  `sup_izq_8` varchar(45) DEFAULT NULL,
  `inf_der_8` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `banner`
--

INSERT INTO `banner` (`id`, `url_imagen_1`, `url_icon_1`, `url_link_1`, `promo_1`, `url_imagen_2`, `url_icon_2`, `url_link_2`, `promo_2`, `url_imagen_3`, `url_icon_3`, `url_link_3`, `promo_3`, `imagen_1`, `link_imagen_1`, `imagen_2`, `link_imagen_2`, `imagen_3`, `link_imagen_3`, `imagen_4`, `link_imagen_4`, `imagen_5`, `link_imagen_5`, `sup_izq_1`, `inf_der_1`, `sup_izq_2`, `inf_der_2`, `sup_izq_3`, `inf_der_3`, `sup_izq_4`, `inf_der_4`, `sup_izq_5`, `inf_der_5`, `sup_izq_6`, `inf_der_6`, `sup_izq_7`, `inf_der_7`, `sup_izq_8`, `inf_der_8`) VALUES
(100, 'nuevo-slider02.png', 'img-miniatura-banner-2.png', 'http://ticketing.selvaviva.cl/', 'bci.png', 'cumple2.jpg', 'img-miniatura-3.png', 'http://selvaviva.cl/cumpleannos.php', 'texto01.png', 'nuevo-slider01.png', 'btn003.png', 'http://selvaviva.cl/horarios.php', 'texto2_3.png', 'btn-compra-entradas.png', 'http://ticketing.selvaviva.cl/', 'golegios02.png', 'http://selvaviva.cl/colegios.php', 'cumple.png', 'http://www.selvaviva.cl/cumpleannos.php', 'TARIFAS.png', 'http://selvaviva.cl/horarios.php', 'miembro-alpza.png', 'http://www.alpza.com', 'sup_izq_mariposario.png', 'fotos-esquina.png', 'sup_01.png', 'inf_03.png', 'sup_02.png', 'inf_04.png', 'sup_04.png', 'inf_02.png', 'sup_izq.png', 'inf_01.png', 'sup_05.png', 'inf_08.png', 'sup_03.png', 'inf_07.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenidos`
--

CREATE TABLE IF NOT EXISTS `contenidos` (
  `id` int(11) NOT NULL,
  `html_proyecto` text,
  `html_selva` text,
  `html_mariposario` text,
  `img_m1` varchar(45) DEFAULT NULL,
  `img_m2` varchar(45) DEFAULT NULL,
  `img_m3` varchar(45) DEFAULT NULL,
  `html_trabajo` text,
  `html_horarios` text,
  `html_precios` text,
  `html_forma_pago` text,
  `html_programa` text,
  `html_actividades` text,
  `html_cumpleanos` text,
  `pdf_actividades` text,
  `html_colegios` text,
  `html_visitas` text,
  `html_empresas` text,
  `correo_destino` varchar(128) DEFAULT 'null',
  `correo_contacto` varchar(128) DEFAULT 'null',
  `correo_reserva` varchar(128) DEFAULT NULL,
  `link_fb` varchar(128) DEFAULT NULL,
  `link_tw` varchar(128) DEFAULT NULL,
  `link_ig` varchar(128) DEFAULT NULL,
  `link_yt` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contenidos`
--

INSERT INTO `contenidos` (`id`, `html_proyecto`, `html_selva`, `html_mariposario`, `img_m1`, `img_m2`, `img_m3`, `html_trabajo`, `html_horarios`, `html_precios`, `html_forma_pago`, `html_programa`, `html_actividades`, `html_cumpleanos`, `pdf_actividades`, `html_colegios`, `html_visitas`, `html_empresas`, `correo_destino`, `correo_contacto`, `correo_reserva`, `link_fb`, `link_tw`, `link_ig`, `link_yt`) VALUES
(1000, 'Selva Viva es el Ãºnico proyecto en Chile, que incorpora en un solo edificio, de casi 1000 m2, a una selva tropical, en todo su esplendor, con el Ãºnico Mariposario del paÃ­s. Visitar Selva Viva es transportarse por un instante, al corazÃ³n de la selva tropical, en donde la alta humedad y temperatura constantes, permiten el abundante desarrollo de exÃ³ticas plantas y animales.<br>El proyecto Selva Viva busca generar en cada uno de sus visitantes, la experiencia de pertenencia con la naturaleza y el planeta en general, fomentando las acciones destinadas a la conservaciÃ³n de ecosistemas naturales, mediante diversos programas de educaciÃ³n e investigaciÃ³n.<br><br>', '<p>\r\n    <b>Selva Viva</b> es la Ãºnica selva tropical indoor presente en Chile, recreada para que el visitante tenga una experiencia fascinante MEDIANTE TODOS SUS SENTIDOS, internÃ¡ndose como un explorador, para descubrir increÃ­bles especies de origen exÃ³tico y conocer directamente, la impresionante vegetaciÃ³n tropical, su temperatura, humedad, sonidos y aromas.</p>', 'Distintas especies de mariposas tropicales revolotean libremente alrededor de los visitantes de Selva Viva, generando de esta forma una experiencia inolvidable. Las mariposas se posan sobre los visitantes, las flores y toman agua en las pequeÃ±as cascadas de Selva Viva. AsÃ­ podrÃ¡n conocer desde enormes mariposas de intenso color azul, a diminutos especÃ­menes, de vivos colores, como un pequeÃ±o ejemplo de todas las especies que podrÃ¡s observar directamente en nuestro Mariposario.<br> Destacamos tambiÃ©n a la famosa mariposa bÃºho y mariposa hoja, entre tantas otras que podrÃ¡s disfrutar en Selva Viva.<br><br><br>', 'm011.png', 'm02.png', 'm03.png', '<div>Las postulaciones estÃ¡n cerradas, gracias.</div><div>Saludos desde la selva!</div>', 'Lunes a viernes de 09:00 a 18:00 hrs.<br>SÃ¡bados, domingos y festivos de 10:00 a 19:00 hrs.<br><br>Venta de entradas hasta agotar stock.<br>* El primer lunes de cada mes Selva Viva estarÃ¡ cerrado por mantenciÃ³n.<br><br>La boleterÃ­a cierra a las 18:00 hrs. los dÃ­as de semana. Los sÃ¡bados, domingos y festivos a las 19:00 hrs. <br><br><br>', '<table width="100%">\r\n    <tbody><tr>\r\n        <td>\r\n            NiÃ±os de 3 a 17 aÃ±os.<br>\r\n            (Menores de 3 aÃ±os no paga)<br><br>\r\n            Adulto de 18 a 59 aÃ±os.<br><br>\r\n            Tercera Edad de 60 aÃ±os y mas.<br>\r\n        </td>\r\n        <td>\r\n            $ 8.950<br><br><br>\r\n            $ 9.950<br><br>\r\n            $ 8.950\r\n        </td>\r\n    </tr>\r\n</tbody></table>', 'Efectivo, Tarjeta de CrÃ©dito, Tarjeta de DÃ©bito.<br>Si es cliente Bci, TBanc o Bci Nova, pagando con su Tarjeta de CrÃ©dito o DÃ©bito obtendrÃ¡ un 20% de descuento en el valor de la entrada.<br><br>Si quieres venir con tu Colegio o InstituciÃ³n <a target="" title="" href="http://www.selvaviva.cl/reserva.php">pincha aquÃ­</a>.<br>*RestricciÃ³n: No se permite el ingreso de menores de 14 aÃ±os sin un adulto<br>', '<p>Selva Viva te invita a actividades durante todo el aÃ±o a travÃ©s del calendario que puedes ver a continuaciÃ³n.<br>Te iremos informando en detalle por medio de la web, todos los eventos que se realizarÃ¡n en cada una de estas fechas:<br><br></p>\r\n<table id="programa">\r\n<tbody><tr>\r\n    <td>\r\n        <ul>\r\n            <li>Enero  / Febrero</li>\r\n            <li>Marzo</li>\r\n            <li>Abril</li>\r\n            <li>Mayo</li>\r\n            <li>Junio</li>\r\n            <li>Julio</li>\r\n            <li>Agosto</li>\r\n            <li>Septiembre</li>\r\n            <li>Octubre</li>\r\n            <li>Noviembre</li>\r\n            <li>Diciembre</li>\r\n        </ul>\r\n    </td>\r\n    <td>\r\n        <ul>\r\n            <li>Vacaciones de verano</li>\r\n            <li>Vuelta a clases</li>\r\n            <li>DÃ­a de la tierra</li>\r\n            <li>Mes del mar</li>\r\n            <li>DÃ­a mundial del medio ambiente</li>\r\n            <li>Vacaciones de invierno</li>\r\n            <li>Semana del niÃ±o</li>\r\n            <li>Mes de la Patria</li>\r\n            <li>DÃ­a Mundial de los animales<br></li>\r\n            <li>DÃ­a Mundial de la ecologÃ­a<br></li>\r\n            <li>Actividades de fin de aÃ±o</li>\r\n        </ul>\r\n    </td>\r\n</tr>\r\n</tbody></table>', 'Descubre y sorpr&#233;ndete con todas las actividades que tenemos preparadas para ti y tu familia. Actividades Indoor y Outdoor que acompa&#241;adas del esp&#237;ritu aventurero de aprender, entretendr&#225;n a grandes y chicos.<br><br><b>Talleres de Fin de Semana</b><br>Durante la visita a Selva Viva, podr&#225;s recorrer libremente y disfrutar de variadas actividades, dise&#241;adas especialmente para ti y tu familia, conoce mediante un contacto directo y cercano, diferentes especies de la flora y fauna, t&#237;picas de la selva tropical.<br>&#161;Aprendamos juntos de forma entretenida!<br>Ven a visitarnos el fin de semana y participa en nuestros entretenidos talleres.<br><br><b>Eventos y Promociones</b><br>En Selva Viva ponemos a su total disposici&#243;n la maravillosa experiencia del ambiente tropical para que puedas celebrar cumplea&#241;os, lanzamientos de productos y eventos especiales.<br><br><p>\r\n    Para inscripciones y mÃ¡s informaciÃ³n escrÃ­benos a <font color="#009933">eventos@selvaviva.cl</font>\r\n</p>\r\n', '<div align="center"><img alt="" src="http://selvaviva.cl/imgs/cumple.png" align="none"></div><p style="margin-bottom: 13.5px;">En Selva Viva podr&#225;s celebrar el cumplea&#241;os m&#225;s entretenido y aventurero que te puedas imaginar. Un gu&#237;a explorador te acompa&#241;ar&#225; a ti y tus amigos durante toda la visita a Selva Viva, donde podr&#225;s caminar por la Selva, pasar por debajo de una cascada de agua, interactuar con asombrosos animales y plantas carn&#237;voras.</p><p style="margin-bottom: 13.5px;">Para los m&#225;s valientes, tenemos la sala de los sentidos, donde podr&#225;n interactuar con incre&#237;bles especies de animales como ara&#241;as pollito, reptiles, ranas, as&#237; como tambi&#233;n podr&#225;n tocar pieles, p&#250;as, plumas, huevos de dinosaurio y f&#243;siles.</p><p style="margin-bottom: 13.5px;">Tambi&#233;n podr&#225;s convertirte junto a tus amigos en Guardianes del Planeta y recibir&#225;n un diploma con su nombre para celebrarlo. Porque un a&#241;o m&#225;s no deber&#237;a celebrarse como siempre, te invitamos a celebrar y descubrir un cumplea&#241;os distinto en Selva Viva.</p><p style="margin-bottom: 13.5px;">Para inscripciones y m&#225;s informaci&#243;n escr&#237;benos a eventos@selvaviva.cl</p><br>', 'Cumple2015.pdf', '<b>Selva Viva</b> ofrece la mejor forma de completar y complementar la misiÃ³n que llevan a cabo en sus colegios profesores y docentes. La aventura Selva Viva, nutre la experiencia del conocimiento en niÃ±os y adultos de una forma inclusiva y dinÃ¡mica, apoyando una conducta positiva hacia el medio ambiente.<br>Todos los grupos de estudiantes que participen de este programa recorrerÃ¡n el circuito con un guÃ­a y dos profesores del colegio visitante en forma permanente.<br>Para visitas o convenios con Establecimientos Educacionales llamar al telÃ©fono: 29446300 o escribir al mail: <font color="#009933">contacto@selvaviva.cl<br></font><br><b>Horario:</b><br>De Lunes a viernes de 9:00 a 17:30 horas. (cerrado primer lunes de cada mes).<br><br><br><br><br>', '<p>\r\n    <b>Selva Viva</b> desarrolla contenidos educativos de ciencias de la naturaleza, adecuados a todos los niveles escolares, mediante variadas actividades didÃ¡cticas, accesibles e interactivas. Nuestros guÃ­as educativos responderÃ¡n las consultas de niÃ±os y niÃ±as, exponiendo clara y sencillamente, sobre aspectos interesantes acerca de las diferentes especies presentes en Selva Viva, conociendo el hÃ¡bitat caracterÃ­stico de una selva tropical y fomentando el cuidado del medio ambiente, complementando de esta manera, los contenidos relacionados con el currÃ­culum escolar.</p>', 'Descubre todo lo que Selva Viva puede ofrecer para tu empresa.<br>Convenios especiales, visitas en grupo, eventos, lanzamientos y aniversarios.<br>Si tu empresa tiene dentro de sus actividades lanzamientos de nuevos productos relacionados con ecologÃ­a, naturaleza, educaciÃ³n, reciclaje y alimentaciÃ³n sana, puedes utilizar nuestros espacios y sala multiuso para estas actividades.<br>Para mayor informaciÃ³n, pinchar aquÃ­: <font color="#009933">eventos@selvaviva.cl<br></font><br>', 'mzavala@selvaviva.cl;contacto@selvaviva.cl', 'contacto@selvaviva.cl;selvaviva14@gmail.com', 'mzavala@selvaviva.cl;contacto@selvaviva.cl', 'https://www.facebook.com/selvavivachile', 'https://twitter.com/selvavivastgo', 'https://instagram.com/selvaviva', 'https://www.youtube.com/selvavivachile');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE IF NOT EXISTS `documentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `documento` varchar(64) DEFAULT NULL,
  `descripcion` varchar(64) DEFAULT NULL,
  `activo` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `documentos`
--

INSERT INTO `documentos` (`id`, `documento`, `descripcion`, `activo`) VALUES
(4, 'material_990620.pdf', 'Visitas Educativas Selva Viva 2015', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE IF NOT EXISTS `equipo` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 NOT NULL,
  `cargo` varchar(100) CHARACTER SET utf8 NOT NULL,
  `descripcion` varchar(500) CHARACTER SET utf8 NOT NULL,
  `foto` varchar(300) NOT NULL,
  `orden` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`id`, `nombre`, `cargo`, `descripcion`, `foto`, `orden`) VALUES
(1, 'TEODORO WIGODSKI', 'PRESIDENTE DIRECTORIO', 'Ingeniero Industrial de la Universidad de Chile MBA, MBL, MAE AcadÃ©mico y Director de Empresas.', 'equipo_1.png', 1),
(5, 'EDUARDO ERGAS', 'DIRECTOR', 'Ingeniero Comercial, Empresario, Presidente de Empresas y Fundaciones.', 'eduardoergas-52193.jpg', 0),
(6, 'BELÃ‰N SEREZAL', 'DIRECTORA', 'Empresaria, Directora de Empresas.', 'BelÃ©n-Serezal-Directora-de-Mundo-Vivo.-220x180-69696.jpg', 0),
(7, 'PATRICIO ELGUETA', 'GERENTE GENERAL', 'Administrador PÃºblico de la Universidad de Chile, MagÃ­ster en PlanificaciÃ³n Tributaria de la UAI, Director Ejecutivo de Conpat.', 'Patricio-Elgueta-Gerente-General-220x180-92308.jpg', 0),
(8, 'PATRICIO VEGA', 'GERENTE DE ADMINISTRACIÃ“N Y FINANZAS', 'Ingeniero en AdministraciÃ³n de Empresas, de amplia experiencia en AdministraciÃ³n de Proyectos.', 'patricioveg-220x180-42572.jpg', 0),
(9, 'ALFREDO UGARTE', 'DIRECTOR CIENTÃFICO', 'Ingeniero AgrÃ³nomo, entomÃ³logo con gran experiencia en crianza y mantenciÃ³n de mariposas en cautiverio. Amplia experiencia en control integrado de plagas. Coautor del libro Las Mariposas de Chile. MÃ¡s de 12 aÃ±os como rostro en diferentes programas televisivos.', '01-28575.png', 0),
(10, 'PEDRO MOHANA ', 'ACCIONISTA', 'Ingeniero Comercial, Director y Fundador de Aquarium Santiago en MIM, Director del Proyecto Animales Asombrosos, 15 aÃ±os de experiencia en proyectos educativos y amplio conocimiento en proyectos interactivos, producciÃ³n de Cine 3D, entre otros.', '02-32348.png', 0),
(11, 'SEBASTÃAN BERNALES', 'ACCIONISTA', 'Ph.D. en BiologÃ­a Celular en la Universidad de California en San Francisco. Investigador Asociado FundaciÃ³n Ciencia para la Vida y Millennium Institute for Applied Biology.', '03-31913.png', 0),
(12, 'JÃ›RGEN ROTTMANN', 'MÃ‰DICO VETERINARIO', 'Profesor universitario. Especialidad en crianza de aves, peces y plantas tropicales en general.', '04-19714.png', 0),
(13, 'EVELYN PEÃ‘A', 'MÃ‰DICO VETERINARIA', 'Especialista en manejo y crianza de Mariposas, con experiencia en Australia y Costa Rica.', '05-88081.png', 0),
(17, 'ALICIA SOLIS ', 'MÃ‰DICO VETERINARIA', 'Profesora Universitaria. Diplomada en Manejo y Medicina de Aves Ornamentales en Universidad Mayor. MÃ©dico Veterinaria a cargo en "FundaciÃ³n Centro de AclimataciÃ³n ZoolÃ³gica ".', '06-53464.png', 0),
(18, 'HERNÃN LORCA ', 'CURADOR GENERAL DEL PROYECTO', 'Profesor de EducaciÃ³n TÃ©cnico Profesional, Jefe de Campo del Programa Binacional de ConservaciÃ³n del CÃ³ndor Andino. Especialista en manejo de fauna silvestre en cautiverio.', '08-32127.png', 0),
(19, 'FERNANDO KOGAN ', 'ACCIONISTA', 'Ingeniero Comercial, Director de Empresa.', '09-11792.png', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria`
--

CREATE TABLE IF NOT EXISTS `galeria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Volcado de datos para la tabla `galeria`
--

INSERT INTO `galeria` (`id`, `imagen`) VALUES
(2, 'imagen781702.png'),
(4, 'imagen991115.png'),
(5, 'imagen356356.png'),
(6, 'imagen191814.png'),
(7, 'imagen593606.png'),
(8, 'imagen392021.png'),
(9, 'imagen507800.png'),
(10, 'imagen781542.png'),
(11, 'imagen757931.png'),
(12, 'imagen125871.png'),
(13, 'imagen800306.png'),
(26, 'imagen483427.png'),
(27, 'imagen332938.png'),
(28, 'imagen628818.png'),
(29, 'imagen156324.png'),
(30, 'imagen657289.png'),
(31, 'imagen831417.png'),
(32, 'imagen851773.png'),
(33, 'imagen145191.png'),
(34, 'imagen703682.png'),
(35, 'imagen136597.png'),
(36, 'imagen522467.png'),
(37, 'imagen305773.png'),
(38, 'imagen415700.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mapas`
--

CREATE TABLE IF NOT EXISTS `mapas` (
  `id` int(11) NOT NULL,
  `mapa` varchar(45) DEFAULT NULL,
  `mapa_n1_small` varchar(45) DEFAULT NULL,
  `mapa_n1` varchar(45) DEFAULT NULL,
  `mapa_n2_small` varchar(45) DEFAULT NULL,
  `mapa_n2` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mapas`
--

INSERT INTO `mapas` (`id`, `mapa`, `mapa_n1_small`, `mapa_n1`, `mapa_n2_small`, `mapa_n2`) VALUES
(1000, 'mapa.png', 'dentro-01.png', 'foto-01-dentro.png', 'dentro-02.png', 'foto-02-dentro.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `foto` varchar(50) NOT NULL,
  `fecha` datetime NOT NULL,
  `activo` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `descripcion`, `foto`, `fecha`, `activo`) VALUES
(1, 'Consejos para el cuidado con los insectos ', 'Al llegar el verano, junto a las altas temperaturas, los insectos comienzan a moverse mucho mÃ¡s, en especial en nuestros hogares, es por eso que siempre debemos estar informado respecto a que debemos prevenir y saber a la hora de alguna picadura o mordedura de mosquitos, algÃºn arÃ¡cnido, etc.Muchos de estos insectos pueden ser peligrosos para los niÃ±os es por eso que nuestro Director CientÃ­fico de Selva Viva, Alfredo Ugarte, el conocido BichÃ³logo, nos comparte una informaciÃ³n importante para nuestras familias, con algunos sencillos consejos que nos podrÃ¡n ayudar a estar mÃ¡s tranquilos y poder cuidar a nuestros seres queridos.', 'Untitled-1-600x300-80011.png', '2015-01-08 00:00:00', 1),
(2, 'Â¡Celebra un cumpleaÃ±os inolvidable!', 'Ven a celebrar con tus mejores amigos de la aventura y  diversiÃ³n en Selva Viva este verano.PodrÃ¡s participar de entretenidas actividades y descubrir las maravillas que esconde nuestra selva tropicalElige tu celebraciÃ³n Â¡y a disfrutar!CumpleaÃ±os TuracoValor por niÃ±o: $13.900Incluye:-Invitaciones digitales-GuÃ­a y Anfitriona-Taller interactivo ï¿½\Ztoca-tocaï¿½\Z-Sala de cumpleaÃ±os decorada-Sticker identificaciÃ³n para todos los invitados-Diploma para todos los invitados-Pulsera Selva Viva para todos los invitados-Torta de cuchuflÃ­ con velas-JugoCumpleaÃ±os TucÃ¡nValor por niÃ±o: $17.900Incluye:-Invitaciones digitales-GuÃ­a y Anfitriona-Taller interactivo ï¿½\Ztoca-tocaï¿½\Z-Sticker identificaciÃ³n para todos los invitados-Diploma para todos los invitados-Pulsera Selva Viva para todos los invitados-Sala de cumpleaÃ±os decorada-Regalo sorpresa para el festejado-Torta de cuchuflÃ­ con velas-Jugo-Brochetas de Marshmallows-Barras de cereal-SÃ¡ndwich triangulo jamÃ³n quesoHaz tu reserva en eventos@mundo-vivo.cl y/0 mzavala@mundo-vivo.cl', 'Banner1-4686.png', '2014-11-15 14:04:23', 0),
(3, 'Â¡Se agranda la familia!', 'Terminamos el aÃ±o llenos de buenas noticias: desde este viernes 05 de diciembre podrÃ¡s visitar en Selva Viva a 3 de los camaleones mÃ¡s grandes del mundo.Â¡AsÃ­ es! A nuestros queridos amigos Velados, se suman los camaleones Gigante o Melleri y Pantera. Con colores impresionantes, ojos que se mueven de manera independiente y uno de ellos con un falso cuerno no dejarÃ¡s de sorprenderte cuando los veas.Y si vienes a las 11:00 y a las 16:00 Â¡podrÃ¡s ver como se alimentan!', 'Banner-3-37002.png', '2014-04-18 14:27:33', 0),
(6, 'Mariposas en Selva Viva', 'PrÃ³ximamente mÃ¡s informaciÃ³n\r\nÂ¡Te esperamos!', '15junio-75050.png', '2015-06-15 18:31:33', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sponsors`
--

CREATE TABLE IF NOT EXISTS `sponsors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(128) DEFAULT NULL,
  `tipo` varchar(32) DEFAULT NULL,
  `icono` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Volcado de datos para la tabla `sponsors`
--

INSERT INTO `sponsors` (`id`, `url`, `tipo`, `icono`) VALUES
(16, '', 'patrocinador', 'sponsor164887.png'),
(17, '', 'patrocinador', 'sponsor876130.png'),
(18, '', 'patrocinador', 'sponsor206789.png'),
(19, '', 'patrocinador', 'sponsor924517.jpg'),
(27, ' http://www.caja18.cl ', 'sponsor', 'sponsor650979.png'),
(29, ' http://fmdos.cl ', 'sponsor', 'sponsor359406.png'),
(30, ' http://www.umayor.cl/um/ ', 'sponsor', 'sponsor472037.png'),
(31, ' http://www.bci.cl/personas/ ', 'sponsor', 'sponsor818890.png'),
(32, ' http://www.revista1320.com ', 'partner', 'sponsor839239.png'),
(33, ' http://www.mega.cl/home/ ', 'partner', 'sponsor951732.png'),
(35, 'https://www.facebook.com/Revista.MuyInteresante.Chile?fref=ts', 'partner', 'sponsor955775.png'),
(36, 'http://www.directv.cl/', 'sponsor', 'sponsor522391.png');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
