<?php session_start();?>
<?php include_once("includes/Consultas.php");?>
<?php include_once("includes/funciones.php");?>
<?php
$con=new Consultas();
if ($_POST) {
    if(isset($_POST["tipo_item"])){
        agregar_item_banner();
    }else{
        agregar_link();
    }
}
    $con= new Consultas();
    $bnr=$con->get_banners();
//$lst=$con->get_lst_Banner_Big("_esp");
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
    <script>
        function borrar(id)
        {
            document.fr.id.value=id;
            if(confirm("Esta seguro que desea eliminar este Banner?")){
                document.fr.submit();
            }
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-xs-7 col-xs-offset-3">
        <h3>Sliders</h3>
    </div>
    <div class="col-xs-1" style="padding-top:15px">
        <!--button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('banner_big_esp.php','_self','')">Agregar</button-->
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=10 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">
        <table class="table table-hover">
            <thead>
            <tr style="background: #9acfea">
                <td>#</td>
                <td style="width: 15%; text-align: center" colspan="2">Imagen</td>
                <td style="width: 15%; text-align: center" colspan="2">Icono</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Fondo 1</td>
                <td style="text-align: center">
                    <img src="../imgs/banners/<?php echo $bnr['url_imagen_1']?>" style="width: 140px"></td>
                <td>
                    <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                        <label for="img_1">Cambiar Imagen (1920 X 1200)</label>
                        <input type="file" name="imagen">
                        <input type="hidden" name="tipo_item" value="url_imagen_1">
                        <button type="submit" class="btn btn-default" name="btn">Asignar Fondo</button>
                    </form>
                </td>
                <!--------------------------------------------------------------------------------------------------------------------->
                <td style="text-align: center"><img src="../imgs/banners/<?php echo $bnr['url_icon_1']?>" style="width: 40px"></td></td>
                <td>
                    <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                        <label for="icon_1">Cambiar Icono (66 X 66)</label>
                        <input type="file" name="imagen">
                        <input type="hidden" name="tipo_item" value="url_icon_1">
                        <button type="submit" class="btn btn-default" name="btn">Asignar Icono</button>
                    </form>
                </td>
            </tr>
            <?php /**/?>
            <tr>
                <td>Logo Promo 1</td>
                <td style="text-align: center; background-color: #cccccc">
                    <img src="../imgs/banners/<?php echo $bnr['promo_1']?>" style="width: 140px"></td>
                <td colspan="3">
                    <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                        <label for="img_1">Cambiar Promocion (480 X 302)</label>
                        <input type="file" name="imagen">
                        <input type="hidden" name="tipo_item" value="promo_1">
                        <button type="submit" class="btn btn-default" name="btn">Asignar Promocion</button>
                    </form>
                </td>
            </tr>

            <tr>
                <td colspan="5" style="vertical-align: bottom">
                    <form class="form-horizontal" role="form" method="post" action="">
                        <div class="form-group">
                            <label for="url_link_1'" class="col-xs-3  control-label">URL del Link 1</label>
                            <div class="col-xs-7 ">
                                <input type="text" class="form-control" id="contenido'" name="contenido" placeholder="http://" value="<?php echo $bnr['url_link_1']?>">
                                <input type="hidden" name="seccion" value="url_link_1">
                            </div>
                            <div class="col-xs-2 ">
                                <button id="update" type="submit" class="btn btn-default">Actualizar</button>
                            </div>
                        </div>
                    </form>
                    <br>
                </td>
            </tr>

            <!--------------------------------------------------------------------------------------------------------------------->
            <!--------------------------------------------------------------------------------------------------------------------->
            <tr>
                <td>Fondo 2</td>
                <td style="text-align: center">
                    <img src="../imgs/banners/<?php echo $bnr['url_imagen_2']?>" style="width: 140px"></td>
                <td>
                    <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                        <label for="img_1">Cambiar Imagen (1920 X 1200</label>
                        <input type="file" name="imagen">
                        <input type="hidden" name="tipo_item" value="url_imagen_2">
                        <button type="submit" class="btn btn-default" name="btn">Asignar Fondo</button>
                    </form>
                </td>
                <!--------------------------------------------------------------------------------------------------------------------->
                <td style="text-align: center"><img src="../imgs/banners/<?php echo $bnr['url_icon_2']?>" style="width: 40px"></td></td>
                <td>
                    <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                        <label for="icon_1">Cambiar Icono (66 X 66)</label>
                        <input type="file" name="imagen">
                        <input type="hidden" name="tipo_item" value="url_icon_2">
                        <button type="submit" class="btn btn-default" name="btn">Asignar Icono</button>
                    </form>
                </td>
            </tr>
            <?php /* */?>
            <tr>
                <td>Logo Promo 2</td>
                <td style="text-align: center; background-color: #cccccc">
                    <img src="../imgs/banners/<?php echo $bnr['promo_2']?>" style="width: 140px"></td>
                <td colspan="3">
                    <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                        <label for="img_1">Cambiar Promocion (480 X 302)</label>
                        <input type="file" name="imagen">
                        <input type="hidden" name="tipo_item" value="promo_2">
                        <button type="submit" class="btn btn-default" name="btn">Asignar Promocion</button>
                    </form>
                </td>
            </tr>

            <tr>
                <td colspan="5" style="vertical-align: bottom">
                    <form class="form-horizontal" role="form" method="post" action="">
                        <div class="form-group">
                            <label for="url_link_2'" class="col-xs-3  control-label">URL del Link 2</label>
                            <div class="col-xs-7 ">
                                <input type="text" class="form-control" id="contenido'" name="contenido" placeholder="http://"  value="<?php echo $bnr['url_link_2']?>">
                                <input type="hidden" name="seccion" value="url_link_2">
                            </div>
                            <div class="col-xs-2 ">
                                <button id="update" type="submit" class="btn btn-default">Actualizar</button>
                            </div>
                        </div>
                    </form>
                    <br>
                </td>
            </tr>
            <!--------------------------------------------------------------------------------------------------------------------->
            <!--------------------------------------------------------------------------------------------------------------------->
            <tr>
                <td>Fondo 3</td>
                <td style="text-align: center">
                    <img src="../imgs/banners/<?php echo $bnr['url_imagen_3']?>" style="width: 140px"></td>
                <td>
                    <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                        <label for="img_3">Cambiar Imagen (1920 X 1200</label>
                        <input type="file" name="imagen">
                        <input type="hidden" name="tipo_item" value="url_imagen_3">
                        <button type="submit" class="btn btn-default" name="btn">Asignar Fondo</button>
                    </form>
                </td>

                <td style="text-align: center"><img src="../imgs/banners/<?php echo $bnr['url_icon_3']?>" style="width: 40px"></td></td>
                <td>
                    <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                        <label for="icon_1">Cambiar Icono (66 X 66)</label>
                        <input type="file" name="imagen">
                        <input type="hidden" name="tipo_item" value="url_icon_3">
                        <button type="submit" class="btn btn-default" name="btn">Asignar Icono</button>
                    </form>
                    <hr>
                </td>
            </tr>
            <?php /* */?>
            <tr>
                <td>Logo Promo 3</td>
                <td style="text-align: center; background-color: #cccccc">
                    <img src="../imgs/banners/<?php echo $bnr['promo_3']?>" style="width: 140px"></td>
                <td colspan="3">
                    <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                        <label for="img_1">Cambiar Promocion (480 X 302)</label>
                        <input type="file" name="imagen">
                        <input type="hidden" name="tipo_item" value="promo_3">
                        <button type="submit" class="btn btn-default" name="btn">Asignar Promocion</button>
                    </form>
                </td>
            </tr>

            <tr>
                <td colspan="5" style="vertical-align: bottom">
                    <form class="form-horizontal" role="form" method="post" action="">
                        <div class="form-group">
                            <label for="url_link_1'" class="col-xs-3  control-label">URL del Link 3</label>
                            <div class="col-xs-7 ">
                                <input type="text" class="form-control" id="contenido'" name="contenido" placeholder="http://"  value="<?php echo $bnr['url_link_3']?>">
                                <input type="hidden" name="seccion" value="url_link_3">
                            </div>
                            <div class="col-xs-2 ">
                                <button id="update" type="submit" class="btn btn-default">Actualizar</button>
                            </div>
                        </div>
                    </form>
                </td>
            </tr>





            <?php

          /*  foreach($lst as $item){ ?>
            <tr>
                <td><?php echo $item['url']?></td>
                <td style="text-align: center"><img src="../img/banner_big/<?php echo $item['imagen']?>" style="width: 120px"></td>
                <td style="text-align: center"><input type="checkbox" disabled <?php echo ($item["activo"]== 1) ? "checked ":"";?>></td>
                <td style="text-align: center"><a href="banner_big_esp.php?id=<?php echo $item["id"]?>"><img src="img/edit_icon.png"></a></td>
                <td style="text-align: center"><a href="javascript:borrar('<?php echo $item["id"]?>')"><img src="img/delete_icon.png"></a></td>
            </tr>
            <?php } */?>
            </tbody>
        </table>
    </div>
</div>


<form name="fr" method="post" action="">
    <input type="hidden" name="id">
    <input type="hidden" name="modo" value="delete">
</form>

</body>
</html>



