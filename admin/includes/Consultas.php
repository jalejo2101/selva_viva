<?php

/**
 * Created by Juan Alejo.
 * User: juan
 * Date: 20/3/15
 * Time: 12:05 PM
 */
error_reporting(0);
require_once('DBManager.php');

class Consultas extends DBManager{

    var $con;
    function  Consultas(){
        $this->con=new DBManager;
    }


    function grabar($sql){
        if($this->con->conectar()==true){
            mysql_query($sql);
        }
    }

    /*************************************************
    * Ejecucion de consulta Generica
    *************************************************/
    public function consulta_sql($sql){
        $result=null;
        if($this->con->conectar()==true){
            $result=mysql_query($sql);
            return $result;
        }
    }

    /*************************************************
     * Retorna los datos del usuario segun su nombre y
     * password dentro de la BD
     ************************************************/
    public function validar_usuario($user,$pass){
    $sql="SELECT * FROM admin WHERE user='$user' AND password='$pass'";
    $res=$this->consulta_sql($sql);
    $lista=array();
    while($row=mysql_fetch_assoc($res))
    {
        $lista[]=$row;
    }

    return $lista[0];
    }










    /*************************************************
     * Retorna las imagenes de la galeria
     ************************************************/
    public function get_imagenes_galeria(){
        $sql="SELECT * FROM galeria";
        $res=$this->consulta_sql($sql);
        $lista=array();
        while($row=mysql_fetch_assoc($res))
        {
            $lista[]=$row;
        }
        //echo $sql;
        return $lista;
    }
    /*************************************************
     * Retorna las imagenes para el banner de inicio
     ************************************************/
    public function get_banners(){
        $sql="SELECT * FROM banner WHERE id=100";
        $res=$this->consulta_sql($sql);
        $lista=array();
        while($row=mysql_fetch_assoc($res))
        {
            $lista[]=$row;
        }
        //echo $sql;
        return $lista[0];
    }
    /*************************************************
     * Retorna las imagenes de la galeria
     ************************************************/
    public function get_imagenes(){
        $sql="SELECT * FROM galeria";
        $res=$this->consulta_sql($sql);
        $lista=array();
        while($row=mysql_fetch_assoc($res))
        {
            $lista[]=$row;
        }
        //echo $sql;
        return $lista;
    }

    /*************************************************
     * Retorna los miembros del equipo
     ************************************************/
    public function get_equipo(){
        $sql="SELECT * FROM equipo";
        $res=$this->consulta_sql($sql);
        $lista=array();
        while($row=mysql_fetch_assoc($res))
        {
            $lista[]=$row;
        }
        //echo $sql;
        return $lista;
    }

    /*************************************************
     * Retorna los mapas
     ************************************************/
    public function get_mapas(){
        $sql="SELECT * FROM mapas WHERE id=1000";
        $res=$this->consulta_sql($sql);
        $lista=array();
        while($row=mysql_fetch_assoc($res))
        {
            $lista[]=$row;
        }
        //echo $sql;
        return $lista[0];
    }

    /*************************************************
     * Retorna los PDFs
     ************************************************/
    public function get_documentos(){
        $sql="SELECT * FROM documentos";
        $res=$this->consulta_sql($sql);
        $lista=array();
        while($row=mysql_fetch_assoc($res))
        {
            $lista[]=$row;
        }
        //echo $sql;
        return $lista;
    }

    /*************************************************
     * Retorna las Noticias
     ************************************************/
    public function get_noticias(){
        $sql="SELECT * FROM noticias";
        $res=$this->consulta_sql($sql);
        $lista=array();
        while($row=mysql_fetch_assoc($res))
        {
            $lista[]=$row;
        }
        //echo $sql;
        return $lista;
    }




    /*************************************************
     * Retorna la Noticia segun su Id
     ************************************************/
    public function get_noticia($id){
        $sql="SELECT * FROM noticias where id=$id";
        $res=$this->consulta_sql($sql);
        $lista=array();
        while($row=mysql_fetch_assoc($res))
        {
            $lista[]=$row;
        }
        //echo $sql;
        return $lista[0];
    }


    /*************************************************
     * Retorna el contenido de las secciones de
     * Proyecto,
     * Selva Viva,
     * Mariposario,
     * Trabaja con nosotros
     ************************************************/
    public function get_contenidos(){
        $sql="SELECT * FROM contenidos WHERE id=1000";
        $res=$this->consulta_sql($sql);
        $lista=array();
        while($row=mysql_fetch_assoc($res))
        {
            $lista[]=$row;
        }
        //echo $sql;
        return $lista[0];
    }




    /*************************************************
     * Retorna los sponsors existentes
     ************************************************/
    public function get_sponsors($tipo=1){
        if($tipo==1){
            $sql="SELECT * FROM sponsors order by tipo asc";
        }else{
            $sql="SELECT * FROM sponsors where tipo='$tipo' order by tipo asc";
        }
        $res=$this->consulta_sql($sql);
        $lista=array();
        while($row=mysql_fetch_assoc($res))
        {
            $lista[]=$row;
        }
        //echo $sql;
        return $lista;
    }



    /*************************************************
    * Retorna un ARRAY con la lista de categorias
    *************************************************
    public function get_categorias(){
        $sql="SELECT categoria FROM categorias";
        $res=$this->consulta_sql($sql);
            $lista=array();
            while( $row=mysql_fetch_assoc($res))
            {
                $lista[]=$row["categoria"];
            }
        return $lista;
    }


    /*************************************************
     * Retorna un INT con el id de la categorias
     *************************************************
    public function get_id_categorias($categoria){
        $sql="SELECT idcat FROM categorias where categoria='$categoria'";
        $res=$this->consulta_sql($sql);

        $row=mysql_fetch_array($res);

        return $row[0];
    }

    /*************************************************
     * Retorna un RESULT un lista de categorias
     * con sus atributos
     *************************************************
    public function get_lista_categorias(){
        $sql="SELECT * FROM categorias";
        $res=$this->consulta_sql($sql);
        $lista=array();
        while($row=mysql_fetch_assoc($res))
        {
            $lista[]=$row;
        }
        return $lista;
    }
    */





}






?>