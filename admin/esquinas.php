<?php session_start();?>
<?php include_once("includes/Consultas.php");?>
<?php include_once("includes/funciones.php");?>
<?php
$con=new Consultas();
if ($_POST) {
    if(isset($_POST["tipo_item"])){
        agregar_item_banner();
    }else{
        agregar_link();
    }
}
    $con= new Consultas();
    $bnr=$con->get_banners();
//$lst=$con->get_lst_Banner_Big("_esp");
?>
<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
    <?php include("head.php");?>
    <title></title>
    <script>
        function borrar(id)
        {
            document.fr.id.value=id;
            if(confirm("Esta seguro que desea eliminar este Banner?")){
                document.fr.submit();
            }
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-xs-7 col-xs-offset-3">
        <h3>Esquinas</h3>
    </div>
    <div class="col-xs-1" style="padding-top:15px">
        <!--button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('banner_big_esp.php','_self','')">Agregar</button-->
    </div>
</div>
<?php
$tit=Array("Que es Selva Viva","Como LLegar","Cumpleaños","Colegios","Empresas","Noticias","Contacto");
?>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=14 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">
        <table class="table">
            <thead>
            <tr style="background: #9acfea">
                <td style="width: 15%; text-align: center" colspan="2">Superior Izquierda</td>
                <td style="width: 15%; text-align: center" colspan="2">Inferior Derecha</td>
            </tr>
            </thead>
            <tbody>
            <?php for($i=0; $i<7; $i++){?>
                <tr>
                    <td colspan="4"><br><strong>Seccion:</strong> <?php echo $tit[$i]?></td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        <img src="../imgs/banners/<?php echo $bnr['sup_izq_'.($i+1)]?>" style="width: 100px"></td>
                    <td>
                        <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                            <label for="imagen">Cambiar Imagen (577 X 630)</label>
                            <input type="file" name="imagen">
                            <input type="hidden" name="tipo_item" value="sup_izq_<?php echo $i+1?>">
                            <button type="submit" class="btn btn-default" name="btn">Asignar Imagen</button>
                        </form>
                    </td>
                    <!--------------------------------------------------------------------------------------------------------------------->
                    <td style="text-align: center">
                        <img src="../imgs/banners/<?php echo $bnr['inf_der_'.($i+1)]?>" style="width: 100px"></td>
                    <td>
                        <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                            <label for="imagen">Cambiar Imagen (407 X 396)</label>
                            <input type="file" name="imagen">
                            <input type="hidden" name="tipo_item" value="inf_der_<?php echo $i+1?>">
                            <button type="submit" class="btn btn-default" name="btn">Asignar Imagen</button>
                        </form>
                    </td>
                </tr>



            <!--------------------------------------------------------------------------------------------------------------------->
            <!--------------------------------------------------------------------------------------------------------------------->
            <?php } ?>

            </tbody>
        </table>
    </div>
</div>


<form name="fr" method="post" action="">
    <input type="hidden" name="id">
    <input type="hidden" name="modo" value="delete">
</form>

</body>
</html>




