<?php session_start();?>
<?php include_once("includes/Consultas.php");?>
<?php include_once("includes/funciones.php");?>
<?php
$con=new Consultas();
if ($_POST) {
    if(isset($_POST["tipo_item"])){
        agregar_item_banner();
    }else{
        agregar_link();
    }
}
    $con= new Consultas();
    $bnr=$con->get_banners();

$size=array("(374 X 426)","(493 X 347)","(489 X 347)","(380 X 161)","(377 X 174)");

?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
    <script>
        function borrar(id)
        {
            document.fr.id.value=id;
            if(confirm("Esta seguro que desea eliminar este Banner?")){
                document.fr.submit();
            }
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-xs-7 col-xs-offset-3">
        <h3>Banners</h3>
    </div>
    <div class="col-xs-1" style="padding-top:15px">
        <!--button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('banner_big_esp.php','_self','')">Agregar</button-->
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=12 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">
        <table class="table table-hover">
            <thead>
            <tr style="background: #9acfea">
                <td style="width: 120px">#</td>
                <td style=" text-align: center" colspan="2">Imagen</td>

            </tr>
            </thead>
            <tbody>

            <?php for($i=1; $i<=5; $i++){?>
            <tr>
                <td style="width: 100px">Banner <?php echo $i?></td>
                <td style="text-align: center; background-color: #cccccc">
                    <img src="../imgs/banners/<?php echo $bnr['imagen_'.$i]?>" style="width: 140px"></td>
                <td>
                    <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                        <label for="img_1">Cambiar Banner <?php echo $size[ $i-1 ]?></label>
                        <input type="file" name="imagen">
                        <input type="hidden" name="tipo_item" value="imagen_<?php echo $i?>">
                        <button type="submit" class="btn btn-default" name="btn">Asignar Banner</button>
                    </form>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align: bottom">
                    <form class="form-horizontal" role="form" method="post" action="">
                        <div class="form-group">
                            <label for="url_link_<?php echo $i?>'" class="col-xs-3  control-label">URL de la Imagen <?php echo $i?></label>
                            <div class="col-xs-7 ">
                                <input type="text" class="form-control" id="contenido'" name="contenido" placeholder="http://" value="<?php echo $bnr['link_imagen_'.$i]?>">
                                <input type="hidden" name="seccion" value="link_imagen_<?php echo $i?>">
                            </div>
                            <div class="col-xs-2 ">
                                <button id="update" type="submit" class="btn btn-default">Actualizar</button>
                            </div>
                        </div>
                    </form>
                    <br>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>


<form name="fr" method="post" action="">
    <input type="hidden" name="id">
    <input type="hidden" name="modo" value="delete">
</form>

</body>
</html>




