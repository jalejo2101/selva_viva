<?php
	require_once('php/httpheaders.php');
	require_once('php/phpmailer/class.phpmailer.php');
	if(count($_POST)>0){
        $seccion = $_POST['seccion'];
        $email = $_POST['email'];
        $mailto = explode(";",$_POST['emailto']);
        $device = $_POST['device'];
        $nombre = $_POST['nombre'];
        $telefono = "";
        $colegio = "";
        $comuna = "";
        if ($seccion == "Tarifas" || $seccion =="Reserva"){
            $telefono = $_POST['telefono'];
            $colegio = $_POST['colegio'];
            $comuna = $_POST['comuna'];
        }
        $mensaje = $_POST['mensaje'];
		$mail = new PHPMailer();
		$mail->CharSet = 'utf-8';
		$mail->From = $email;
		$mail->FromName = "Selva - ".$seccion;
		$mail->Subject = "Formulario ".$seccion;
        foreach($mailto as $m) {
            $mail->AddAddress($m);
        }
        $body = '<!doctype html>
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <title>TU PREMIO</title>
        </head>
        <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Georgia, Times, serif; background-color:#fff;">
         <h1><img src="http://principal-desarrollo.com/desarrollo/selva/imgs/logo_selva.png" ></h1>
         <h3>'.$seccion.'</h3>
         <ul>
            <li>'.$nombre.'</li>
            <li>'.$email.'</li>
            <li>'.$telefono.'</li>
            <li>'.$colegio.'</li>
            <li>'.$comuna.'</li>
            <li>'.$mensaje.'</li>
         </ul>
        </body>
        </html>';
        $mail->IsHTML(true);
        $mail->Body = $body;
        $seccion=strtolower($seccion).".php";
        if($result = $mail->Send()){
            if($device == "desktop")
                 header('Location: '.$seccion);
            else if($mobile == "desktop")
                 header('Location: m/index.php');
        }
        else{
            if($device == "desktop")
                 header('Location: index.php?err=1');
            else if($mobile == "desktop")
                 header('Location: m/index.php?err=1');
        }
	}
	else{
?>
<h1>Error Del Sistema.</h1>
<?php
	} // header('Location: sorteo.php');
?>
