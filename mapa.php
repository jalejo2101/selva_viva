<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <?php include_once ("includes/head.php") ?>
    <?php $mp=$con->get_mapas()?>
    <title></title>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#tab_1").click(function(){
                $("#tab_1").css("background-image","url('imgs/btn-mapa-hover.png')");
                $("#tab_2").css("background-image","url('imgs/btn-dentro.png')");
                $("#panel1").css("display","block");
                $("#panel2").css("display","none");
            });
            $("#tab_2").click(function(){
                $("#tab_1").css("background-image","url('imgs/btn-mapa.png')");
                $("#tab_2").css("background-image","url('imgs/btn-dentro-hover.png')");
                $("#panel1").css("display","none");
                $("#panel2").css("display","block");
            });
        });

        (function($){
            $(window).load(function(){
                $("#text").mCustomScrollbar({
                    theme:"dark"
                });
            });
        })(jQuery);
    </script>
</head>
<body>
 <div id="principal">
    <!---------------------------------------------------------------------------->

     <div  id="img_sup_izq">
         <img src="imgs/banners/<?php echo $bnr["sup_izq_2"]?>"/>
     </div>
     <div  id="img_inf_der">
         <img src="imgs/banners/<?php echo $bnr["inf_der_2"]?>"/>
     </div>



    <!---------------------------------------------------------------------------->
    <div id="header">
        <?php
        $op_m=3;
        include_once("includes/menu_header.php");
        ?>
    </div>

    <div id="main">
        <div class="margen-sup"></div>
            <div id="frame">
                <table border="0">
                    <tr>
                        <td valign="top" align="right">
                            <?php
                            $op="2";
                            include_once("includes/menu_comollegar.php")?>
                        </td>
                        <td>
                            <div id="text">
                                <h2>Mapa del Parque</h2>
                                <p>
                                    Encontrar la Selva es muy Facil!
                                </p>
                                <div id="temp"></div>
                                <table id="mapa" border="0">
                                    <tr>
                                        <td id="tab_1" style="max-width: 275px; min-width: 275px"></td>
                                        <td id="tab_2" style="max-width: 275px; min-width: 275px"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="min-width: 275px">
                                            <div id="panel1">
                                            <a href="imgs/mapas/<?php echo $mp["mapa"]?>"" data-lightbox="Galeria">
                                                <img src="imgs/mapas/<?php echo $mp["mapa"]?>" alt="" width="510px"/>
                                                </a>
                                                <img src="imgs/direccion.png" alt="" width="85%"/>
                                                
                                            </div>
                                            
                                            
                                            <div id="panel2">
                                                <a href="imgs/mapas/<?php echo $mp["mapa_n1"]?>"" data-lightbox="Galeria">
                                                    <img src="imgs/mapas/<?php echo $mp["mapa_n1_small"]?>" style="width: 510px" alt=""/></a>
                                                <br>
                                                <a href="imgs/mapas/<?php echo $mp["mapa_n2"]?>" data-lightbox="Galeria">
                                                    <img src="imgs/mapas/<?php echo $mp["mapa_n2_small"]?>" style="width: 510px" alt=""/></a>
                                                <br>

                                                <img src="imgs/direccion.png" alt="" width="85%"/>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
    </div>
    <div id="footer">
        <table><tr><td align="left" valign="bottom">
            <?php include_once("includes/footer_promo.php")?>
        </td></tr></table>
    </div>
 </div>
<script type="text/javascript">
    $(document).ready(function(){
        var x= $(window).height();
        $("#text").height(x-250);
        $(window).resize(function(){
            //$("#temp").text($("#text").height());
            var x= $(window).height();
            $("#text").height(x-240);

           //alert("ok");
        });
    });
</script>

</body>
</html>


