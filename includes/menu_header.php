<?php if(!isset($op_m))
    $op_m=0;
?>
<div id="logo"><a href="http://www.selvaviva.cl"><img src="imgs/logo-selva_LT.png" alt=""/></a></div>
<div id="redes">
    <img src="imgs/siguenos.png" style="margin: 3px 0px 0px 20px"/>
    <div>
        <a href="<?php echo $contenido["link_fb"]?>" target="_blank"><img src="imgs/btn-fb.png" alt=""/></a>
        <a href="<?php echo $contenido["link_tw"]?>" target="_blank"><img src="imgs/btn-tw.png" alt=""/></a>
        <a href="<?php echo $contenido["link_ig"]?>" target="_blank"><img src="imgs/btn-inst.png" alt=""/></a>
        <a href="<?php echo $contenido["link_yt"]?>" target="_blank"><img src="imgs/btn-youtube.png" alt=""/></a>
    </div>
</div>
<div id="menu-h">
    <a href="index.php"><div class="m-item" id="btn-home"
            <?php echo ($op_m==1)?'style="background-image: url(imgs/btn-home-in.png)"': ' '?>></div></a>
    <a href="proyecto.php"><div class="m-item" id="btn-que_es"
            <?php echo ($op_m==2)?'style="background-image: url(imgs/btn-que-es-in.png)"': ' '?>></div></a>
    <a href="mapa.php"><div class="m-item" id="btn-como_llegar"
            <?php echo ($op_m==3)?'style="background-image: url(imgs/btn-como-in.png)"': ' '?>></div></a>
    <a href="actividades.php"><div class="m-item" id="btn-actividades"
            <?php echo ($op_m==4)?'style="background-image: url(imgs/btn-actividades-in.png)"': ' '?>></div></a>
    <a href="colegios.php"><div class="m-item" id="btn-colegios"
            <?php echo ($op_m==5)?'style="background-image: url(imgs/btn-colegios-in.png)"': ' '?>></div></a>
    <a href="empresas.php"><div class="m-item" id="btn-empresas"
            <?php echo ($op_m==6)?'style="background-image: url(imgs/btn-empresas-in.png)"': ' '?>></div></a>
    <a href="noticias.php"><div class="m-item" id="btn-noticias"
            <?php echo ($op_m==7)?'style="background-image: url(imgs/btn-noticias-in.png)"': ' '?>></div></a>
    <a href="contacto.php"><div class="m-item" id="btn-contacto"
            <?php echo ($op_m==8)?'style="background-image: url(imgs/btn-contacto-in.png)"': ' '?>></div></a>

</div>