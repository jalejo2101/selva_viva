<meta charset="utf-8">
<link rel="icon" type="image/png" href="imgs/favicon.ico" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.backstretch.min.js"></script>
<script src="js/jquery.mCustomScrollbar.js"></script>
<script src="js/lightbox.js"></script>
<link rel="stylesheet" href="css/style.css"/>
<link rel="stylesheet" href="css/lightbox.css"/>
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" />
<title>Selva Viva</title>

<script>
    (function($){
        $(window).load(function(){
            $("#text").mCustomScrollbar({
                theme:"dark"
            });
        });
    })(jQuery);
</script>
<?php include_once ("admin/includes/Consultas.php") ?>
<?php
$con = new Consultas();
$contenido=$con->get_contenidos();

$bnr=$con->get_banners();
$lst_spn=$con->get_sponsors("sponsor");
$lst_ptr=$con->get_sponsors("patrocinador");
$lst_prt=$con->get_sponsors("partner");
?>