<?php 
    require_once("includes/top.html");

?>
<script>
    function showhide(pos){
        
        if ( $("#horariosList dd:nth-child("+pos+")").is(":visible") ) {
            $("#horariosList dd.open").slideUp();
            $("#horariosList dd.open").removeClass("open");
        }
        else{
        $("#horariosList dd:nth-child("+pos+")").slideDown();
        $("#horariosList dd:nth-child("+pos+")").addClass("open");
        }
        //$("#horariosList dd:nth-child("+pos+")").switchClass( "big", "blue", 1000, "easeInOutQuad" );
        
        $( "#horariosList dt.off:nth-child("+(pos-1)+")" ).switchClass( "off", "on", 1000 );
        $( "#horariosList dt.on:nth-child("+(pos-1)+")" ).switchClass( "on", "off", 1000 );
    }

    $(document).ready(function() {
        $("#menuLateral ul li:nth-child(5) a").css("background","#3C7E24");
    });
    
    function carga(pagina){
        $(".seccion").load(pagina+".php", function() {
          newAltura = $('#allWrapper').height(); 
          $('#menuLateral').height(newAltura+60);
        });
    }
</script>

    <div id="content">
        <h1>Colegios</h1>
        <ul class="submenu">
            <li><a href="#" onclick="carga('tarifas')">Tarifas</a></li>
            <li><a href="#" onclick="carga('visitas')">Visitas Guiadas</a></li>
            <li><a href="#" onclick="carga('reserva')">Reserva tu visita</a></li>
            <li><a href="#" onclick="carga('contenidos')">Contenidos para el Profesor</a></li>
        </ul>
        
        <section class="seccion">
            <h3>Colegios</h3>
            <p>Selva Viva ofrece la mejor forma de completar y complementar la misión que llevan a cabo en sus colegios profesores y docentes. La aventura Selva Viva nutre la experiencia del conocimiento en niños y adultos de una forma inclusiva y dinámica, apoyando una conducta positiva hacia el medio ambiente.</p>

<p>Todos los grupos de estudiantes que participen de este programa recorrerán el circuito con un guía y dos profesores del colegio visitante en forma permanente.</p>

<p>Para convenios con Establecimientos Educacionales llamar al teléfono 29446300 o escribir al mail :   contacto@mundo-vivo.cl</p>

<p>Horario:</p>

<p>De Lunes a viernes de 9:00 a 17:30 horas. (cerrado primer lunes de cada mes)</p>

<!-- <p><span>Completando el siguiente formulario podrás obtener toda la información necesaria para coordinar una visita guiada para grupos de estudiantes.</span></p>
            <div class="arrow"><img src="imgs/greenarrow.png"></div> -->
        </section>
    </div>
    

<?php require_once("includes/bottom.html") ?>