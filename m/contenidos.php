<h3>Contenidos para el Profesor</h3>
<p>¡ Pongamos en práctica lo que aprendimos!</p>

<p>Contamos con guías educativas según su nivel (1º básico a 4º medio). En ellas se da información sobre la naturaleza y los seres vivos y además posee una sección de  ejercicios donde el alumno podrá  poner en práctica sus conocimientos.</p>

<p>Este material podrá estar a disposición del profesor a través de una solicitud directa vía mail una vez inscrito en la web.</p>

<div class="whiteForm">
    <form>
        <input type="text" >
    </form>
    <a class="descargar">Descargar</a>
</div>