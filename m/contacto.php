<?php 
    require_once("includes/top.html");
    require_once("includes/conn.php"); 

    $sqlcont = "SELECT correo_contacto FROM contenidos";
    $rescont = mysql_query($sqlcont); 

?>

<script>
        function envia()
        {
            if(confirm("Desea enviar correo?")){
                document.enviamail.submit();
            }
        }
</script>

<script src="http://maps.google.com/maps/api/js"> </script>
<script>
    $(document).ready(function() {
        $("#menuLateral ul li:nth-child(6) a").css("background","#3C7E24");
        var mapOptions = {
        center: new google.maps.LatLng( -33.468176600000000000, -70.749864600000020000),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP};
        var map = new google.maps.Map(document.getElementById("mapa_ubicacion"),mapOptions); 
    });
</script>

    <div id="content">
        <h1>Contacto</h1>
        <section class="seccion">
            <p>Si tienes cualquier duda o consulta, te invitamos a ponerte en contacto con nosotros a través de nuestro formulario de contacto o vía telefónica.</p>
        </section>
        
        <section id="formulario">
            <form>
                <input type="text" name="nombre" placeholder="Nombre">
                <input type="text" name="email" placeholder="E-mail">
                <section id="elTextarea"><textarea  name="mensaje"></textarea> <div id="camaleon"></div></section>
                
                <input type="hidden" name="seccion" value="Contacto">
                <input type="hidden" name="emailto" value="<?php echo mysql_result($rescont, 0, "correo_contacto"); ?>">
                <input type="hidden" name="device" value="mobile">
            </form>
            <a href="#" class="enviar" onclick="envia()">ENVIAR</a>
        </section>
        <section id="ubicacion">
            <div id="mapa_ubicacion"> 
            </div>
            
            <div class="direcciones">
                <ul>
                    <li>Avenida Presidente Riesco #5330, Las Condes, Santiago, Chile. Parque Araucano.</li>
                    <li>+56 2 29446 300</li>
                    <li>contacto@selvaviva.cl</li>
                </ul>
            </div>
        </section>
    </div>
    

<?php require_once("includes/bottom.html") ?>