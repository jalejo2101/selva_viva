<?php 
    require_once("includes/top.html");

?>
<script>
    function showhide(pos){
        
        if ( $("#horariosList dd:nth-child("+pos+")").is(":visible") ) {
            $("#horariosList dd.open").slideUp();
            $("#horariosList dd.open").removeClass("open");
        }
        else{
        $("#horariosList dd:nth-child("+pos+")").slideDown();
        $("#horariosList dd:nth-child("+pos+")").addClass("open");
        }
        //$("#horariosList dd:nth-child("+pos+")").switchClass( "big", "blue", 1000, "easeInOutQuad" );
        
        $( "#horariosList dt.off:nth-child("+(pos-1)+")" ).switchClass( "off", "on", 1000 );
        $( "#horariosList dt.on:nth-child("+(pos-1)+")" ).switchClass( "on", "off", 1000 );
    }

    $(document).ready(function() {
        $("#menuLateral ul li:nth-child(3) a").css("background","#3C7E24");
    });
    
    function carga(pagina){
        $(".seccion").load(pagina+".php", function() {
          newAltura = $('#allWrapper').height(); 
          $('#menuLateral').height(newAltura+60);
        });
    }

</script>

    <div id="content">
        <h1>¿cómo llegar?</h1>
        <ul class="submenu">
            <li><a onclick="carga('mapa')" href="#">Mapa del Parque</a></li>
            <li><a onclick="carga('horarios')" href="#">Horarios y Tarifas</a></li>
            <li><a onclick="carga('programa')" href="#">Programa</a></li>
        </ul>
        
        <section class="seccion">
            <h3>Mapa del Parque</h3>
            <section id="mapaParque">

                <ul id="tabs">
                    <li class="activo" onclick="mapas('out');"><p>Mapa</p></li>
                    <li class="noactivo" onclick="mapas('in');"><p>Dentro de Selva Viva</p></li>
                </ul>
                <div id="mapOut" class="mapacont">
                    <img src="imgs/mapaparque.png">
                    <ul>
                        <li>Avenida Presidente Riesco #5330, Las Condes, Santiago, Chile. Parque Araucano.</li>
                        <li>+56 2 29446 300</li>
                        <li>contacto@selvaviva.cl</li>
                    </ul>
                </div>

                <div id="mapIn" class="mapacont">
                    <img src="imgs/mapa01-dentro.png">
                    <img src="imgs/mapa02-dentro.png">
                </div>

            </section>

        <script>
            function mapas(show){
                if(show=="in"){
                    $("#mapOut").fadeOut();
                    $("#mapIn").delay( 400 ).fadeIn();
                    $("#tabs li:first-child").removeClass("activo");
                    $("#tabs li:first-child").addClass("noactivo");
                    $("#tabs li:last-child").removeClass("noactivo");
                    $("#tabs li:last-child").addClass("activo");

                }
                if(show=="out"){
                    $("#mapIn").fadeOut();
                    $("#mapOut").delay( 400 ).fadeIn();
                    $("#tabs li:first-child").removeClass("noactivo");
                    $("#tabs li:first-child").addClass("activo");
                    $("#tabs li:last-child").removeClass("activo");
                    $("#tabs li:last-child").addClass("noactivo");
                }   
            }
        </script>
        </section>
    </div>
    

<?php require_once("includes/bottom.html") ?>
