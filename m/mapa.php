<h3>Mapa del Parque</h3>
<section id="mapaParque">

    <ul id="tabs">
        <li class="activo" onclick="mapas('out');"><p>Mapa</p></li>
        <li class="noactivo" onclick="mapas('in');"><p>Dentro de Selva Viva</p></li>
    </ul>
    <div id="mapOut" class="mapacont">
        <img src="imgs/mapaparque.png">
        <ul>
            <li>Avenida Presidente Riesco #5330, Las Condes, Santiago, Chile. Parque Araucano.</li>
            <li>+56 2 29446 300</li>
            <li>contacto@selvaviva.cl</li>
        </ul>
    </div>
    
    <div id="mapIn" class="mapacont">
        <img src="imgs/mapa01-dentro.png">
        <img src="imgs/mapa02-dentro.png">
    </div>
    
</section>

<script>
    function mapas(show){
        if(show=="in"){
            $("#mapOut").fadeOut();
            $("#mapIn").delay( 400 ).fadeIn();
            $("#tabs li:first-child").removeClass("activo");
            $("#tabs li:first-child").addClass("noactivo");
            $("#tabs li:last-child").removeClass("noactivo");
            $("#tabs li:last-child").addClass("activo");
            
        }
        if(show=="out"){
            $("#mapIn").fadeOut();
            $("#mapOut").delay( 400 ).fadeIn();
            $("#tabs li:first-child").removeClass("noactivo");
            $("#tabs li:first-child").addClass("activo");
            $("#tabs li:last-child").removeClass("activo");
            $("#tabs li:last-child").addClass("noactivo");
        }   
    }
</script>