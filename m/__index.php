<?php 
    require_once("includes/top.html");
    require_once("includes/conn.php");  
?>
<script>
    $(document).ready(function() {
        $("#allWrapper").css("background-image","none");
        $("#menuLateral ul li:nth-child(1) a").css("background","#3C7E24");
        $(".s01").fadeIn();
        $("#botonera ul li:nth-child(1)").addClass('bnnractv');
    });
    
    var k=1;
    setInterval( function() { bannerchange(k); k++; if(k>3) k=1; }, 4000 );
    
    function bannerchange(a){
        
        if($(".s01").is(":visible") && a != 1){
            $(".s01").fadeOut();
            $("#botonera ul li:nth-child(1)").removeClass('bnnractv');
        }
        if($(".s02").is(":visible") && a != 2){
            $(".s02").fadeOut();
            $("#botonera ul li:nth-child(2)").removeClass('bnnractv');
        }
        if($(".s03").is(":visible") && a != 3){
            $(".s03").fadeOut();
            $("#botonera ul li:nth-child(3)").removeClass('bnnractv');
        }
        
        $(".s0"+a).fadeIn();
        $("#botonera ul li:nth-child("+a+")").addClass('bnnractv');
    }
    
</script>

<?php
    $sqlbanners = "SELECT * FROM banner";
    $resbanners = mysql_query($sqlbanners); 

    $sqllinks = "SELECT link_fb, link_tw, link_ig, link_yt FROM contenidos";
    $reslinks = mysql_query($sqllinks); 
?>

    <div id="banners">
        <section class="slide s01" style="background-image:url(../imgs/banners/<? echo mysql_result($resbanners, 0, "url_imagen_1"); ?>);"></section>
        <section class="slide s02" style="background-image:url(../imgs/banners/<? echo mysql_result($resbanners, 0, "url_imagen_2"); ?>);"></section>
        <section class="slide s03" style="background-image:url(../imgs/banners/<? echo mysql_result($resbanners, 0, "url_imagen_3"); ?>);"></section>
        
        
            <article><img src="imgs/textobanner.png"></article>
            <nav id="botonera">
                <ul>
                    <li><img src="../imgs/banners/<? echo mysql_result($resbanners, 0, "url_icon_1"); ?>" onclick="bannerchange('1');"></li>
                    <li><img src="../imgs/banners/<? echo mysql_result($resbanners, 0, "url_icon_2"); ?>" onclick="bannerchange('2');"></li>
                    <li><img src="../imgs/banners/<? echo mysql_result($resbanners, 0, "url_icon_3"); ?>" onclick="bannerchange('3');"></li>
                </ul>
            </nav>
        
        <footer id="footbanner"></footer>
    </div>
        <footer id="home-foot">
            <section id="minibanner">
                <a href="http://ticketing.selvaviva.cl"><img src="imgs/minibanner.png"></a>
            </section>
            
            <section id="sociales">
            <h5>Siguenos</h5>
                <ul>
                    <li><a href="<? echo mysql_result($reslinks, 0, "link_fb"); ?>" target="_blank"><img src="imgs/RSFace.png" /></a></li>
                    <li><a href="<? echo mysql_result($reslinks, 0, "link_tw"); ?>" target="_blank"><img src="imgs/RSTwitter.png" /></a></li>
                    <li><a href="<? echo mysql_result($reslinks, 0, "link_ig"); ?>" target="_blank"><img src="imgs/RSInstagram.png" /></a></li>
                    <li><a href="<? echo mysql_result($reslinks, 0, "link_yt"); ?>" target="_blank"><img src="imgs/RSYoutube.png" /></a></li>
                </ul>
            </section>
        </footer>

    </div>

</div>

</body>
</html>
