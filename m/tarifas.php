<?php 
    require_once("includes/conn.php"); 

    $sqlcont = "SELECT correo_destino FROM contenidos";
    $rescont = mysql_query($sqlcont); 

?>

<script>
        function envia()
        {
            if(confirm("Desea enviar correo?")){
                document.enviamail.submit();
            }
        }
</script>

<h3>Tarifas</h3>
<p>Para mayor información escribir a Marisol Zavala: <span>mzavala@mundo-vivo.cl</span></p>
<p>Completando el siguiente formulario podrás obtener toda la información necesaria para coordinar una visita guiada para grupos de estudiantes.</p>

<section id="formulario">
    <form action="../sendmail.php" method="post" id="enviamail" name="enviamail">
        <input type="text" name="nombre" placeholder="Nombre de contacto">
        <input type="text" name="telefono" placeholder="Telefono">
        <input type="text" name="email" placeholder="E-mail" required>
        <input type="text" name="colegio" placeholder="Nombre de Colegio">
        <input type="text" name="comuna" placeholder="Comuna de Colegio">
        
        <input type="hidden" name="seccion" value="Tarifas">
        <input type="hidden" name="emailto" value="<?php echo mysql_result($rescont, 0, "correo_destino"); ?>">
        <input type="hidden" name="device" value="mobile">
        
        <section id="elTextarea"><textarea name="mensaje"></textarea> <div id="camaleon"></div></section>
    </form>
    <a href="#" class="enviar" onclick="envia()">ENVIAR</a>
</section>
