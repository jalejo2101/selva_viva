<?php 
    require_once("includes/top.html");
    require_once("includes/conn.php"); 

    $sqlcont = "SELECT html_actividades FROM contenidos";
    $rescont = mysql_query($sqlcont); 
?>
<script>

    $(document).ready(function() {
        $("#menuLateral ul li:nth-child(4) a").css("background","#3C7E24");
    });

    function carga(pagina){
        $(".seccion").load(pagina+".php", function() {
          newAltura = $('#allWrapper').height(); 
          $('#menuLateral').height(newAltura+60);
        });
    }
</script>

    <div id="content">
        <h1>Actividades y Cumpleaños</h1>
        <ul class="submenu">
            <li><a onclick="carga('actividades')" href="#">Actividades</a></li>
            <li><a onclick="carga('cumples')" href="#">Cumpleaños</a></li>
        </ul>
        
        <section class="seccion">
            <h3>Actividades</h3>
            <?php echo mysql_result($rescont, 0, "html_actividades"); ?>
        </section>
    </div>
    

<?php require_once("includes/bottom.html") ?>