<?php 
    require_once("includes/top.html");

?>
<script>
    function showhide(pos){
        
        if ( $("#horariosList dd:nth-child("+pos+")").is(":visible") ) {
            $("#horariosList dd.open").slideUp();
            $("#horariosList dd.open").removeClass("open");
        }
        else{
        $("#horariosList dd:nth-child("+pos+")").slideDown();
        $("#horariosList dd:nth-child("+pos+")").addClass("open");
        }
        //$("#horariosList dd:nth-child("+pos+")").switchClass( "big", "blue", 1000, "easeInOutQuad" );
        
        $( "#horariosList dt.off:nth-child("+(pos-1)+")" ).switchClass( "off", "on", 1000 );
        $( "#horariosList dt.on:nth-child("+(pos-1)+")" ).switchClass( "on", "off", 1000 );
    }

    $(document).ready(function() {
        $("#menuLateral ul li:nth-child(3) a").css("background","#3C7E24");
    });

</script>

    <div id="content">
        <h1>¿cómo llegar?</h1>
        <ul class="submenu">
            <li><a href="#">Horarios y Tarifas</a></li>
            <li><a href="#">Mapa del Parque</a></li>
            <li><a href="#">Programa</a></li>
        </ul>
        
        <section class="seccion">
            <h3>Horarios y Tarifas</h3>
            <dl id="horariosList">
                <dt class="off" onclick="showhide(2)">Horario</dt>
                <dd>Black hot drink</dd>
                <dt  class="off" onclick="showhide(4)">Precios</dt>
                <dd>White cold drink</dd>
                <dt  class="off" onclick="showhide(6)">Forma de Pago</dt>
                <dd>White cold drink</dd>
            </dl>
        </section>
    </div>
    

<?php require_once("includes/bottom.html") ?>
