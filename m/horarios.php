
            <h3>Horarios y Tarifas</h3>
            <dl id="horariosList">
                <dt class="off" onclick="showhide(2)">Horario</dt>
                <dd>
                <p>Lunes a viernes de 09:00 a 18:00 hrs.</p>
                <p>Sábados y domingos de 10:00 a 19:00 hrs.</p>

                <p>Venta de entradas hasta agotar stock.</p>

                <p>La boletería cierra a las 18:00 hrs. los días de semana y 19:00 hrs.
                sábados y domingos.
                El público que ingresa a las 18:00 hrs. los días de semana y 19:00 hrs.
                los fines de semana, tienen una hora de recorrido (hasta las 20:00 hrs) en el
                interior de Selva Viva</p>
                </dd>
                
                <dt  class="off" onclick="showhide(4)">Tarifas</dt>
                <dd>
                <table class="precios">
                    <tbody>
                        <tr>
                            <td>Niños de 3 a 17 años (Menores de 3 años no paga).</td>
                            <td>$ 8.950</td>
                            
                        </tr>
                        <tr>
                            <td>Adulto de 18 a 59 años.</td>
                            <td>$ 9.950</td>
                        </tr>
                        <tr>
                            <td>Tercera Edad de 60 años y mas.</td>
                            <td>$ 8.950</td>
                        </tr>
                    </tbody>
                </table>
                </dd>
                
                <dt  class="off" onclick="showhide(6)">Forma de Pago</dt>
                <dd>
                    <p>Efectivo, Tarjeta de Crédito, Tarjeta de Débito.</p>

                    <p>Si es cliente Bci, TBanc o Bci Nova, pagando con su
                    Tarjeta de Crédito o Débito obtendrá un 20% de descuento en el
                    valor de la entrada.</p>
                </dd>
            </dl>
