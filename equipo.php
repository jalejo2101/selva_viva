<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <?php include_once ("includes/head.php") ?>
    <?php $lista=$con->get_equipo()?>


    <title></title>
</head>
<body>
 <div id="principal">
    <!---------------------------------------------------------------------------->

     <div  id="img_sup_izq">
         <img src="imgs/banners/<?php echo $bnr["sup_izq_1"]?>"/>
     </div>
     <div  id="img_inf_der">
         <img src="imgs/banners/<?php echo $bnr["inf_der_1"]?>"/>
     </div>


    <!---------------------------------------------------------------------------->
    <div id="header">
        <?php
        $op_m=2;
        include_once("includes/menu_header.php");
        ?>
    </div>
    <div id="main">
        <div class="margen-sup"></div>
            <div id="frame">
                <table border="0">
                    <tr>
                        <td valign="top" align="right">
                            <?php
                            $op="5";
                            include_once("includes/menu.php")
                            ?>
                        </td>
                        <td>
                            <div id="text" style="width: 450px">
                                <?php foreach ($lista as $lst){?>
                                    <div class="equipo">
                                        <div>                                            
                                            <img src="imgs/team/<?php echo $lst["foto"]?>" alt=""/>
                                        </div>
                                        <div>
                                            <strong><?php echo $lst["nombre"]?></strong><BR>
                                            <strong><?php echo $lst["cargo"]?></strong><BR>
                                            <?php echo $lst["descripcion"]?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
    </div>
    <div id="footer">
        <table><tr><td align="left" valign="bottom">
            <?php include_once("includes/footer_promo.php")?>
        </td></tr></table>
    </div>
 </div>
<script type="text/javascript">
    $(document).ready(function(){
        var x= $(window).height();
        $("#text").height(x-250);
        $(window).resize(function(){
            //$("#temp").text($("#text").height());
            var x= $(window).height();
            $("#text").height(x-250);

           //alert("ok");
        });
    });
</script>

</body>
</html>